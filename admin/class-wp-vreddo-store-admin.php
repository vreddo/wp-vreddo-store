<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://pedrops.com
 * @since      1.0.0
 *
 * @package    Wp_Vreddo_Store
 * @subpackage Wp_Vreddo_Store/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Wp_Vreddo_Store
 * @subpackage Wp_Vreddo_Store/admin
 * @author     Pedro Germani <pedro.germani@gmail.com>
 */
class Wp_Vreddo_Store_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->options = get_option($this->plugin_name.'_options');

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wp_Vreddo_Store_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wp_Vreddo_Store_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/wp-vreddo-store-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wp_Vreddo_Store_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wp_Vreddo_Store_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/wp-vreddo-store-admin.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * Register Settings page menu
	 *
	 * @since    1.0.0
	 */
	public function vreddo_store_create_menu() {
		
		
		add_submenu_page(
			'woocommerce',
			__( 'VReddo Store', 'textdomain' ),
			__( 'VReddo Store', 'textdomain' ),
			'manage_options',
			'vreddo-store-settings',
			array($this, 'vreddo_store_create_settings')
		);
	}

	/**
	 * Register Settings page menu
	 *
	 * @since    1.0.0
	 */
	public function vreddo_store_create_settings() {
		// check user capabilities
		if ( ! current_user_can( 'manage_options' ) ) {
			wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
		}

		// add error/update messages
		// check if the user have submitted the settings
		// wordpress will add the "settings-updated" $_GET parameter to the url
		if ( isset( $_GET['settings-updated'] ) ) {
			// add settings saved message with the class of "updated"
			add_settings_error( 'vreddo_store_messages', 'vreddo_store_message', __( 'Settings Saved', $this->plugin_name ), 'updated' );
		}
		
		// show error/update messages		
		settings_errors( 'vreddo_store_messages' );	

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/wp-vreddo-store-admin-display.php';
	}

	public function vreddo_store_register_settings() {
		//register our settings
		register_setting( $this->plugin_name, $this->plugin_name.'_options', array( $this, 'vreddo_store_validate_settings' ) );

		// register a new section in the plugin page
		add_settings_section(
			'vreddo_store_section_settings',
			__( 'Product Category Prices', $this->plugin_name ),
			array( $this ,'vreddo_store_section_settings_cb'),
			$this->plugin_name
		);

		// register a new field in the "vreddo_store_section_settings" section, inside the plugin page
		add_settings_field(
			'vreddo_store_field_enable_category_price',
			__( 'Enable Category Prices', $this->plugin_name ),
			array( $this ,'vreddo_store_field_enable_category_price_cb'),
			$this->plugin_name,
			'vreddo_store_section_settings',
			[
				'label_for' => 'vreddo_store_field_enable_category_price',
				'class' => 'vreddo_store_row',
				'vreddo_store_custom_data' => 'custom',
			]
		);

		// register a new field in the "vreddo_store_section_settings" section, inside the plugin page
		add_settings_field(
			'vreddo_store_field_category_price',
			__( 'Category Prices', $this->plugin_name ),
			array( $this ,'vreddo_store_field_category_price_cb'),
			$this->plugin_name,
			'vreddo_store_section_settings',
			[
				'label_for' => 'vreddo_store_field_category_price',
				'class' => 'vreddo_store_row',
				'vreddo_store_custom_data' => 'custom',
			]
		);
	}

	/**
	 * Callback to print field Title.
	 *
	 * @since    1.0.0
	 */
	function vreddo_store_field_enable_category_price_cb( $args ) {
		// output the field
		var_dump ($this->options); 
		$enable_category_price = $this->options[esc_attr($args['label_for'])];
		if($enable_category_price){ 
			$checked = "checked=\"checked\""; }
		else{ $checked = "";}
		?>
		
		<input type="checkbox" id="<?php echo esc_attr( $args['label_for'] ); ?>" data-custom="<?php echo esc_attr( $args['vreddo_store_custom_data'] ); ?>"
		    name="<?php echo $this->plugin_name.'_options[' . esc_attr($args['label_for']). ']' ?>" value=1 class="" <?php echo $checked;
		    ?> />

		<p class="description">
			<?php esc_html_e( 'Caution! Enabling Product Category Fixed Price will change prices for all products.', $this->plugin_name ); ?>
		</p>
		
		<?php
	}

	/**
	 * Callback to print Category Prices field
	 *
	 * @since    1.0.0
	 */
	function vreddo_store_field_category_price_cb( $args ) {
		// output the field
		$enable_category_price = $this->options[esc_attr('vreddo_store_field_enable_category_price')];
		if($enable_category_price) {
			$product_categories = get_terms(
				array(
					'taxonomy'   => "product_cat",
					'number'     => $number,
					'orderby'    => $orderby,
					'order'      => $order,
					'hide_empty' => $hide_empty,
					'include'    => $ids
				)
			);

			$category_prices = $this->options[esc_attr('vreddo_store_field_category_price')];

			foreach ($product_categories as $product_cat) {
				?>
				<div>
					<div style="width:100px; display: inline-block;">
						<input type="text" 
							id="<?php echo esc_attr( $args['label_for'] ); ?>"
							name="<?php echo $this->plugin_name.'_options[' . esc_attr($args['label_for']). ']['.$product_cat->term_id.']' ?>" 
							value="<?= $category_prices[$product_cat->term_id] ?: ''?>"
							class="" <?php echo $checked;?> 
							size=8
						/>
						
					</div>
					<div style="width:50% ; margin:auto; display: inline;">
						<?= $product_cat->name.' [id:'.$product_cat->term_id.']'; ?>
					</div>
				</div>
				<?php
			}	
		} else {
			echo 'OFF		';
		}

		
	}

	/**
	 * Developer Callback (see https://developer.wordpress.org/plugins/settings/custom-settings-page/)
	 *
	 * @since    1.0.0
	 */
	function vreddo_store_section_settings_cb( $args ) {
		// enqueue style and js for admin settings page
		// wp_enqueue_style( 'wp-color-picker' );
		// wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/breaking-news-toptal-admin.css', array(), $this->version, 'all' );
		// wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/breaking-news-toptal-admin.js', array( 'jquery', 'wp-color-picker' ), $this->version, false );	
		?>
		<p id="<?php echo esc_attr( $args['id'] ); ?>">
			<?php esc_html_e( 'Edit your settings and save.', $this->plugin_name ); ?>
		</p>
		<?php
	}

	/**
	 * Callback to validate plugin settings.
	 *
	 * @since    1.0.0
	 */
	public function vreddo_store_validate_settings( $fields ) { 
		
		$valid_fields = array();
		
		// Validate Title Field
		$enable_category_price = $fields['vreddo_store_field_enable_category_price'];
		if (empty($enable_category_price)) {
			// Set the error message
			add_settings_error( 'vreddo_store_messages', 'vreddo_store_message', __('Attention: Product Category prices Disabled!', $this->plugin_name), 'notice' ); // $setting, $code, $message, $type
			// Get previous value
			$valid_fields['vreddo_store_field_enable_category_price'] = false;
		} else {
			$valid_fields['vreddo_store_field_enable_category_price'] = (bool) $enable_category_price;
		}

		// validate category prices
		$product_cat_prices = $fields['vreddo_store_field_category_price'];
		foreach ($product_cat_prices as $product_cat_price) {
			
		}
		$valid_fields['vreddo_store_field_category_price'] = $product_cat_prices;
		
		return apply_filters( 'vreddo_store_validate_settings', $valid_fields, $fields);
	}

	/**
	 * Fix the product price
	 *
	 * @since    1.0.0
	 */
	public function vreddo_store_fix_product_price($price, $product) {

		// get product category
		$product_cat = get_the_terms( $product->get_id(), 'product_cat' );
		$product_cat_id = $product_cat[0]->term_id;

		// Get price
		$product_cat_price = $this->vreddo_store_get_product_cat_price($product_cat_id);
		
		// set price conditionally to cat prices, else return original price
		if ($product_cat_price > 0) {
			$return_price = $product_cat_price;	
		}
		else {
			$return_price = $price;
		}
		
		return $return_price;
	}
	
	/**
	 * Get Product Category Price by ID
	 *
	 * @since    1.0.0
	 */
	public function vreddo_store_get_product_cat_price ($product_cat_id) {
		// get all category prices
		$category_prices = $this->options[esc_attr('vreddo_store_field_category_price')];
		
		// return only price for product_cat_id in param
		return $category_prices[$product_cat_id];
	}
	
	/**
	 * Sets the product default price to 10. Only works once, if the price is not specified.
	 *
	 * @param int    $post_id
	 * @param object $post
	 */
	public function vreddo_store_wc_set_product_default_price( $data, $post ) {
		if ($post['post_status'] != 'auto-draft') {
			$post_id = $post['ID'];
			$product     = wc_get_product( $post_id );
			$already_set = get_post_meta( $post_id, '_set_default_price', true );
			$price       = $product->get_price();

			$product_cat = get_the_terms( $post_id, 'product_cat' );
			$s = $_POST['cat_term'];
			
			$product_cat_id = $product_cat[0]->term_id;

			$product_cat_price = $this->vreddo_store_get_product_cat_price($product_cat_id);

			//if ( 'yes' !== $already_set && empty( $price )) {
				$product->set_regular_price( $product_cat_price );
				$product->save();

				update_post_meta( $post_id, '_set_default_price', 'yes' );
			//}
		}
		return $data;
	}

	/**
	 * Set virtual and software by default
	 *
	 * @since    1.0.0
	 */
	function vreddo_store_wc_product_type_options( $product_type_options ) {
		$product_type_options['virtual']['default'] = 'yes';
		$product_type_options['downloadable']['default'] = 'yes';	
		return $product_type_options;
	}
	
}
