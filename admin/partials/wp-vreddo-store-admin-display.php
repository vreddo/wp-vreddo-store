<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://pedrops.com
 * @since      1.0.0
 *
 * @package    Wp_Vreddo_Store
 * @subpackage Wp_Vreddo_Store/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<div class="wrap">
	<h1>VReddo Store Settings</h1>
</div>

<div class="wrap">
<form action="options.php" method="POST">
		<?php
		// output security fields for the registered setting "bnpg_options"
		settings_fields($this->plugin_name);
		// output setting sections and their fields
		// (sections are registered for plugin, each field is registered to a specific section)
		do_settings_sections($this->plugin_name);
		// output save settings button
		submit_button('Save Settings');
		?>
	</form>
</div>
