<?php

/**
 * Fired during plugin activation
 *
 * @link       https://pedrops.com
 * @since      1.0.0
 *
 * @package    Wp_Vreddo_Store
 * @subpackage Wp_Vreddo_Store/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Wp_Vreddo_Store
 * @subpackage Wp_Vreddo_Store/includes
 * @author     Pedro Germani <pedro.germani@gmail.com>
 */
class Wp_Vreddo_Store_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
