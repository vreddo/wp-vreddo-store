<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://pedrops.com
 * @since      1.0.0
 *
 * @package    Wp_Vreddo_Store
 * @subpackage Wp_Vreddo_Store/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Wp_Vreddo_Store
 * @subpackage Wp_Vreddo_Store/includes
 * @author     Pedro Germani <pedro.germani@gmail.com>
 */
class Wp_Vreddo_Store_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'wp-vreddo-store',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
